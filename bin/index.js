#!/usr/bin/env node

var base1 = parseInt(process.argv[2]);

//console.log(base1);

if (base1 < 2 || base1 > 36) {
	console.error("Invalid base #1: " + process.argv[2]);
	process.exit(1);
}

var base2 = parseInt(process.argv[4]);

//console.log(base2);

if (base2 < 2 || base2 > 36) {
	console.error("Invalid base #2: " + process.argv[4]);
	process.exit(1);
}

if (process.argv.indexOf('--float') >= 0) {
	var number = parseFloat(process.argv[3].toLowerCase(), base1);
} else {
	var number = parseInt(process.argv[3].toLowerCase(), base1);
}

if (isNaN(number) || number.toString(base1).toLowerCase() != process.argv[3].toLowerCase()) {
	console.error("Could not parse value '" + process.argv[3] + "' in base " + base1);
	process.exit(1);
}

console.log(number.toString(base2).toUpperCase());
process.exit(0);

// Done
